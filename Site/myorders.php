<?php
	include("private/logincheck.php");
	include("private/orderfns.php");
	include("private/general.php");
	$msg = '';
	
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$action    = isset($_POST['action'])?input($_POST['action']):false;
		$orderid   = isset($_POST['orderid'])?input($_POST['orderid']):false;
		$courierid = isset($_POST['courierid'])?input($_POST['courierid']):false;
		
		if($action == false || ($action != 'cancel' && $action != 'delete' && $action != 'undo')) $msg = "The action could not be processed.";
		else if($action == 'cancel' && ($orderid == false || ! ctype_digit($orderid))) $msg = "The orderid specified is invalid.";
		else if(($action == 'delete' || $action == 'undo') && ($courierid == false && ! ctype_digit($courierid))) $msg = "The courierid specified was invalid.";
		else
		{
			if($action == "cancel")
			{
				if(Cancel($orderid))
				{
					header("Location: myorders.php");
					exit;
				}
				else $msg = "There was an error cancelling order #" . $orderid;
			}
			
			if($action == 'delete')
			{
				if(CancelCourier($courierid))
				{
					header("Location: myorders.php");
					exit;
				}
				else $msg = "There was an error cancelling courier #" . $courierid;
			}
			
			if($action == 'undo')
			{
				if(UndoComplete($courierid))
				{
					header("Location: myorders.php");
					exit;
				}
				else $msg = "There was an error undoing the complete on courier #" . $courierid;
			}
		}
	}
	
	try
	{
		$filter = new stdClass();
		$filter->CharacterID = $_SESSION['characterID'];
		$filter->Limit = 10;
		$orders = GetListOfOrders($filter);
		unset($filter);
	}
	catch(Exception $ex)
	{
		$msg = "There was an error getting your orders.";
	}
	
	$filter = new stdClass();
	$filter->CharacterID = $_SESSION['characterID'];
	$filter->Limit = 10;
	$couriers = GetCouriers($filter);
	unset($filter);
	
	if(isJfPilot())
	{
		$filter = new stdClass();
		$filter->JFCharacterID = $_SESSION['characterID'];
		$filter->Completed     = true;
		$filter->Cancelled     = false;
		$filter->Submitted     = true;
		$filter->Limit         = 10;
		$myCompletedOrders = GetListOfOrders($filter);
		unset($filter);
			
		$filter = new stdClass();
		$filter->JFCharacterID = $_SESSION['characterID'];
		$filter->Completed = true;
		$filter->Cancelled = false;
		$filter->Limit     = 10;
		$myCompletedCouriers = GetCouriers($filter);
		unset($filter);
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/general.css" />
		<title>My History</title>
		<link rel="shortcut icon" href="images/favicon.ico" />
	</head>
	<body>
		<div class='gradient'>
			<?php include("private/header.php"); ?>
			<div class="outer">
				<div class="middle">
					<div class="inner">
						<p>Last 10 Orders</p>
						<div class="resultsbox">
							<?php
								if($msg == '' && count($orders) == 0)
								{
									echo "<p>You do not have any orders.</p>";
								}
								else if($msg == '' && count($orders) > 0)
								{
									$colarray = new stdClass();
									$colarray->Comments    = true;
									$colarray->OrderID     = true;
									$colarray->OrderDate   = true;
									$colarray->Status      = true;
									$colarray->OrderType   = true;
									$colarray->JFCharName  = true;
									$colarray->Destination = true;
									$colarray->Price       = true;
									$colarray->CancelOrder = true;
									$colarray->ViewOrder   = true;
									$colarray->Reorder     = true;
									echo "<div class='table'>";
									OutputOrderList($orders, $colarray);
									echo "</div>";
									unset($colarray);
								}
								else echo "<p>" . $msg . "</p>";
							?>
						</div>
					</div>
					<div class='inner'>
						<p>Last 10 Courier Contracts</p>
						<div class='resultsbox'>
							<div class='table'>
								<?php
									if(count($couriers) == 0)
									{
										echo "<p>You have yet to create a courier contract.</p>";
									}
									else if(count($couriers) > 0)
									{
										$filter = new stdClass();
										$filter->Comments        = true;
										$filter->OrderID         = true;
										$filter->Volume          = true;
										$filter->Source          = true;
										$filter->Destination     = true;
										$filter->Cancel          = true;
										$filter->Status          = true;
										$filter->CourierDate     = true;
										$filter->JFCharacterName = true;
										OutputCouriers($couriers, $filter);
										unset($filter);
									}
									else
									{
										echo "<p>There was an error getting your courier contracts.</p>";
									}
								?>
							</div>
						</div>
					</div>
					<?php
						if(isJfPilot())
						{
							echo "<div class='inner'><p>Last 10 Orders Delivered By Me</p><div class='resultsbox'><div class='table'>";
							if(count($myCompletedOrders) == 0)
							{
								echo "<p>You have not completed any orders.</p>";
							}
							else if(count($myCompletedOrders) > 0)
							{
								$cols = new stdClass();
								$cols->Comments      = true;
								$cols->OrderID       = true;
								$cols->CharacterName = true;
								$cols->OrderDate     = true;
								$cols->OrderType     = true;
								$cols->Destination   = true;
								$cols->Volume        = true;
								$cols->Price         = true;
								$cols->ViewOrder     = true;
								OutputOrderList($myCompletedOrders, $cols);
								unset($cols);
							}
							else echo"<p>There was an error fetching your completed orders.</p>";
							echo "</div></div></div>";
							
							echo "<div class='inner'><p>Last 10 Couriers Delivered By Me</p><div class='resultsbox'><div class='table'>";
							if(count($myCompletedCouriers) == 0)
							{
								echo "<p>You have not completed any courier contracts.</p>";
							}
							else if(count($myCompletedCouriers) > 0)
							{
								$filter = new stdClass();
								$filter->Comments      = true;
								$filter->CourierID     = true;
								$filter->Volume        = true;
								$filter->Source        = true;
								$filter->Destination   = true;
								$filter->Complete      = true;
								$filter->CourierDate   = true;
								$filter->CharacterName = true;
								$filter->Undo          = true;
								OutputCouriers($myCompletedCouriers, $filter);
								unset($filter);
							}
							else
							{
								echo "<p>There was an error getting your completed courier contracts.</p>";
							}
							echo "</div></div></div>";
						}
					?>
					<div class='inner'>
						<div class='resultsbox'>
							<p><u>My History</u></p>
							<p>Here you can see the last 10 orders and courier contracts placed by yourself. In both cases, incomplete and non-cancelled items appear at the top of the list with others further down.</p>
							<p>Orders can be cancelled up until a jumpfreighter pilot claims the order for delivery. In order to cancel the order once it is claimed you must notify the jumpfreighter pilot and they must release the order.</p>
				</div>
			</div>
		</div>
	</body>
</html>