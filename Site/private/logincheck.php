<?php
	include("session.php");

	if(isLoggedIn())
	{
		if(! isset($_SESSION['timeout'])) $_SESSION['timeout'] = time();
		
		if($_SESSION['timeout'] + 30 * 60 < time()) logout();
		else $_SESSION['timeout'] = time();
	}
	
	if(! isLoggedIn())
	{
		header("Location: login.php");
		exit;
	}
?>