<?php
	include("database.php");

	function GetCurrentOrderId()
	{
			$selectstring = "SELECT OrderID FROM Orders WHERE CharacterID = " . $_SESSION["characterID"] . " AND Submitted = 0 AND Cancelled = 0";
			
			$con = connect("write");
			$orderid = NULL;
			if($con)
			{
					$results = mysqli_query($con, $selectstring);
					if(mysqli_num_rows($results) == 0)
					{
							$insertstring = "INSERT INTO Orders (CharacterID, Submitted, Completed, Cancelled) VALUES(" . $_SESSION['characterID'] . ", 0, 0, 0)";
							$insertresult = mysqli_query($con, $insertstring);
							
							if(! $insertresult)
							{
									mysqli_free_result($results);
									mysqli_close($con);
									throw new Exception();
							}
							mysqli_free_result($results);
							
							$results = mysqli_query($con, $selectstring);
							$row = mysqli_fetch_array($results);
							$orderid = $row['OrderID'];
					}
					else if(mysqli_num_rows($results) == 1)
					{
							$row = mysqli_fetch_array($results);
							$orderid = $row['OrderID'];
					}
					else
					{
							mysqli_free_result($results);
							mysqli_close($con);
							throw new Exception();
					}
					mysqli_free_result($results);
					mysqli_close($con);
			}
		 return $orderid;
	}

	function Show($val)
	{
		return $val === true;
	}
	
//========== SINGLE ORDER FUNCTIONS ==========
	function GetOrderDetails($orderid)
	{
			if(!ctype_digit($orderid)) return NULL;
			
			$query1 = "SELECT * FROM Orders INNER JOIN Pilots ON Orders.CharacterID = Pilots.CharacterID WHERE OrderID = " . $orderid;
			$query2 = "SELECT * FROM OrderItems INNER JOIN Items ON OrderItems.TypeID = Items.TypeID WHERE OrderID = " . $orderid;
			
			$order = NULL;
			
			$con = connect("read");
			if($con)
			{
					$results = mysqli_query($con, $query1);
					if(mysqli_num_rows($results) != 1)
					{
							mysqli_close($con);
							return NULL;
					}
					
					$row = mysqli_fetch_array($results);
					
					$order = new stdClass();
					$order->OrderID =        $row['OrderID'];
					$order->CharacterID =    $row['CharacterID'];
					$order->CharacterName =  $row["CharacterName"];
					$order->JFCharacterID =  $row['JFCharacterID'];
					$order->Destination =    $row['Destination'];
					$order->Submitted =      $row['Submitted'];
					$order->Completed =      $row['Completed'];
					$order->Cancelled =      $row['Cancelled'];
					$order->OrderDate =      $row['OrderDate'];
					$order->OrderType =      $row['OrderType'];
					$order->Items = Array();
					
					mysqli_free_result($results);
					
					$counter = 0;
					$results = mysqli_query($con, $query2);
					while($row = mysqli_fetch_array($results))
					{
						if(isset($row['TypeID']))
						{
							$order->Items[$counter] = new stdClass();
							$order->Items[$counter]->TypeID          = $row['TypeID'];
							$order->Items[$counter]->ItemName        = $row['ItemName'];
							$order->Items[$counter]->ItemDescription = $row['ItemDescription'];
							$order->Items[$counter]->Volume          = floatval($row['Volume']);
							$order->Items[$counter]->Price           = floatval($row['Price']);
							$order->Items[$counter]->Quantity        = intval($row['Quantity']);
							$counter++;
						}
					}
					mysqli_free_result($results);
					mysqli_close($con);
			}
			
			return $order;
	}
	
	function OutputOrderTitle($order, $colarray)
	{
		if(! isset($colarray->newlines))
		{
			if(isset($colarray->Submit)        && Show($colarray->Submit))         echo "<form action='index.php' method='post'>";
			echo "<div class='table'><div class='table-row'>";
			if(isset($colarray->TitleText))	                                       echo "<div class='table-cell'><p>" . $colarray->TitleText . "</p></div>";
			if(isset($colarray->OrderID)       && Show($colarray->OrderID))        echo "<div class='table-cell'><p>#" . $order->OrderID . "</p></div>";
			if(isset($colarray->CharacterName) && Show($colarray->CharacterName))  echo "<div class='table-cell'><p>" . htmlspecialchars($order->CharacterName, ENT_QUOTES | ENT_HTML401) . "</p></div>";
			echo "<div class='table-cell' style='width:100%;'></div>";
			if(isset($colarray->Destination)   && Show($colarray->Destination))    echo "<div class='table-cell'><p>" . ($order->Destination == null? "" : $order->Destination) . "</p></div>";
			if(isset($colarray->Status)        && Show($colarray->Status))         echo "<div class='table-cell'><p>" . ($order->Cancelled == 1? "Cancelled" : ($order->Completed == 1? "Completed" :($order->Submitted == 1? "Submitted":"Not Submitted"))) . "</p></div>";
			if(isset($colarray->OrderDate)     && Show($colarray->OrderDate))      echo "<div class='table-cell'><p>" . $order->OrderDate . "</p></div>";
			if(isset($colarray->OrderType)     && Show($colarray->OrderType))      echo "<div class='table-cell'><p>" . $order->OrderType . "</p></div>";
			if(isset($colarray->Submit)        && Show($colarray->Submit))
			{
				echo "<div class='table-cell'>";
				PopulateLocationsDropdown();
				echo "<select name='orderType'><option value='personal'>Give to Me</option><option value='market'>Put on Market</option></select><input type='hidden' name='action' value='submit' /><input type='submit' value = 'Checkout'/></div>";
			}
			echo "</div></div>";
			
			if(isset($colarray->Submit)        && Show($colarray->Submit))
			{
				echo "<textarea maxlength='2048' rows='1' placeholder='Comments' name='comments' style='width:100%'></textarea>";
				echo "</form>";
			}
		}
		else
		{
			echo "<div class='table'>";
			if(isset($colarray->TitleText))	                                       echo "<div class='table-row'><div class='table-cell'></div><div class='table-cell' style='width:100%;'><p>" . $colarray->TitleText . "</p></div></div>";
			if(isset($colarray->OrderID)       && Show($colarray->OrderID))        echo "<div class='table-row'><div class='table-cell'><p>Order # </p></div><div class='table-cell' style='width:100%;'><p>" . $order->OrderID . "</p></div></div>";
			if(isset($colarray->CharacterName) && Show($colarray->CharacterName))  echo "<div class='table-row'><div class='table-cell'><p>Placed by </p></div><div class='table-cell' style='width:100%;'><p>" . htmlspecialchars($order->CharacterName, ENT_QUOTES | ENT_HTML401) . "</p></div></div>";
			if(isset($colarray->Destination)   && Show($colarray->Destination))    echo "<div class='table-row'><div class='table-cell'><p>Destination </p></div><div class='table-cell' style='width:100%;'><p>" . ($order->Destination == null? "" : $order->Destination) . "</p></div></div>";
			if(isset($colarray->Status)        && Show($colarray->Status))         echo "<div class='table-row'><div class='table-cell'><p>Status </p></div><div class='table-cell' style='width:100%;'><p>" . ($order->Cancelled == 1? "Cancelled" : ($order->Completed == 1? "Completed" :($order->Submitted == 1? "Submitted":"Not Submitted"))) . "</p></div></div>";
			if(isset($colarray->OrderDate)     && Show($colarray->OrderDate))      echo "<div class='table-row'><div class='table-cell'><p>Date </p></div><div class='table-cell' style='width:100%;'><p>" . $order->OrderDate . "</p></div></div>";
			if(isset($colarray->OrderType)     && Show($colarray->OrderType))      echo "<div class='table-row'><div class='table-cell'><p>Type </p></div><div class='table-cell' style='width:100%;'><p>" . $order->OrderType . "</p></div></div>";
			echo "</div>";
		}
	}
	
	function OutputOrderRows($order, $colarray)
	{
		$orderrows = $order->Items;
		echo "<div class='table-row table-header'>";
		if(isset($colarray->ItemName) && Show($colarray->ItemName)) echo "<div class='table-cell' style='width:100%;'><p>Item</p></div>";
		if(isset($colarray->Price)    && Show($colarray->Price))    echo "<div class='table-cell right-text'><p>Price Per Unit</p></div>";
		if(isset($colarray->Volume)   && Show($colarray->Volume))   echo "<div class='table-cell right-text'><p>Volume</p></div>";
		if(isset($colarray->Quantity) && Show($colarray->Quantity)) echo "<div class='table-cell right-text'><p>Quantity</p></div>";
		if(isset($colarray->Remove)   && Show($colarray->Remove))		echo "<div class='table-cell'></div>";
		echo "</div>";
		$total = 0;
		$qtytot = 0;
		$voltot = 0;
		
		foreach($orderrows as $row)
		{
			echo "<div class='table-row'>";
			if(isset($colarray->ItemName) && Show($colarray->ItemName)) echo "<div class='table-cell clickable' onclick='CCPEVE.showInfo(" . $row->TypeID . ")'><p>" . htmlspecialchars($row->ItemName, ENT_QUOTES | ENT_HTML401) . "</p></div>";
			if(isset($colarray->Price)    && Show($colarray->Price))    echo "<div class='table-cell right-text'><p>" . number_format(floatval($row->Price),2) . " ISK</p></div>";
			if(isset($colarray->Volume)   && Show($colarray->Volume))   echo "<div class='table-cell right-text'><p>" . number_format(floatval($row->Volume),2) . " m&#179;</p></div>";
			if(isset($colarray->Quantity) && Show($colarray->Quantity)) echo "<div class='table-cell right-text'><p>" . number_format(intval($row->Quantity),0) . "</p></div>";
			if(isset($colarray->Remove)   && Show($colarray->Remove) && $order->Submitted == 0 && $order->Cancelled == 0 && $order->Completed == 0 )
																		echo "<div class='table-cell'><form action='index.php' method='post'><input type='hidden' name='typeID' value='" . $row->TypeID ."' /><input type='hidden' name='action' value='remove' /><input type='submit' value='Remove' /></form></div>";
			echo "</div>";
			$total += floatval($row->Price) * intval($row->Quantity);
			$qtytot += intval($row->Quantity);
			$voltot += floatval($row->Volume) * intval($row->Quantity);
		}
		echo "<div class='table-row table-header'>";
		if(isset($colarray->ItemName) && Show($colarray->ItemName)) echo "<div class='table-cell'><p>Total</p></div>";
		if(isset($colarray->Price)    && Show($colarray->Price))    echo "<div class='table-cell right-text'><p>" . number_format($total, 2) . " ISK</p></div>";
		if(isset($colarray->Volume)   && Show($colarray->Volume))   echo "<div class='table-cell right-text'><p>" . number_format($voltot, 2) . " m&#179;</p></div>";
		if(isset($colarray->Quantity) && Show($colarray->Quantity)) echo "<div class='table-cell right-text'><p>" . number_format($qtytot, 0) . "</p></div>";
		if(isset($colarray->Remove)   && Show($colarray->Remove))		echo "<div class='table-cell'></div>";
		echo "</div>";
		
	}

	function Reorder($orderid)
	{
		$Details = GetOrderDetails($orderid);
		if(intval($Details->CharacterID) == intval($_SESSION['characterID']))
		{
			foreach($Details->Items as $Item)
			{
				AddToOrder($Item->TypeID, $Item->Quantity);
			}
		}
	}
	
//========== SINGLE ORDER FUNCTIONS ==========
//========== MULTI ORDER FUNCTIONS ==========
	
	function GetListOfOrders($filter)
	{
		$querystring = "SELECT OrderID, OrderDate, Submitted, Completed, Cancelled, OrderType, Comments, JFPilot.CharacterName AS JFCharName, Buyer.CharacterName AS BuyerName, Destination, Volume, Price FROM (OrderSummary INNER JOIN Pilots AS Buyer ON OrderSummary.CharacterID = Buyer.CharacterID) LEFT JOIN Pilots AS JFPilot ON OrderSummary.JFCharacterID = JFPilot.CharacterID WHERE TRUE";
		if(isset($filter->CharacterID) && ctype_digit($filter->CharacterID)) $querystring .= " AND OrderSummary.CharacterID = " . $filter->CharacterID;
		if(isset($filter->JFCharacterID) && $filter->JFCharacterID == false) $querystring .= " AND JFCharacterID IS NULL ";
		if(isset($filter->JFCharacterID) && ctype_digit($filter->JFCharacterID)) $querystring .= " AND JFCharacterID = " . $filter->JFCharacterID;
		if(isset($filter->Completed)) $querystring .= " AND Completed = " . ($filter->Completed? "1":"0");
		if(isset($filter->Cancelled)) $querystring .= " AND Cancelled = " . ($filter->Cancelled? "1":"0");
		if(isset($filter->Submitted)) $querystring .= " AND Submitted = " . ($filter->Submitted? "1":"0");
		if(isset($filter->OrderType)) $querystring .= " AND OrderType = '" . $filter->OrderType . "'";
		$querystring .= " ORDER BY CASE WHEN Submitted = 0 THEN 0 WHEN Submitted = 1 AND Cancelled = 0 AND Completed = 0 THEN 1 ELSE 2 END, OrderDate";
		if(isset($filter->Limit)) $querystring .= " LIMIT " . $filter->Limit;
		$con = connect("read");
		
		$retval = null;
		if($con)
		{
			$results = mysqli_query($con, $querystring);
			if($results) $retval = Array();
			$counter = 0;
			while($row = mysqli_fetch_array($results))
			{
				if(!empty($row))
				{
					$retval[$counter] = new stdClass();
					$retval[$counter]->Comments =      $row['Comments'];
					$retval[$counter]->OrderID =       $row['OrderID'];
					$retval[$counter]->CharacterName = $row['BuyerName'];
					$retval[$counter]->OrderDate =     $row['OrderDate'];
					$retval[$counter]->Submitted =     $row['Submitted'];
					$retval[$counter]->Completed =     $row['Completed'];
					$retval[$counter]->Cancelled =     $row['Cancelled'];
					$retval[$counter]->OrderType =     $row['OrderType'];
					$retval[$counter]->JFCharName =    $row['JFCharName'];
					$retval[$counter]->Destination =   $row['Destination'];
					$retval[$counter]->Volume =        $row['Volume'];
					$retval[$counter]->Price =         $row['Price'];
					$counter++;
				}
			}
			mysqli_free_result($results);
		}
		mysqli_close($con);
		return $retval;
	}
	
	//array values for colarray
	//OrderID, CharacterName, OrderDate, Status, OrderType, JFCharName, Destination, Volume, Price, CancelOrder, ViewOrder
	function OutputOrderList($orderlist, $colarray)
	{
		echo "<div class='table-row table-header'>";
		if(isset($colarray->Comments)      && Show($colarray->Comments))      echo "<div class='table-cell'></div>";
		if(isset($colarray->OrderID)       && Show($colarray->OrderID))       echo "<div class='table-cell' style='width:100%;'><p>Order ID</p></div>";
		if(isset($colarray->CharacterName) && Show($colarray->CharacterName)) echo "<div class='table-cell right-text'><p>Character</p></div>";
		if(isset($colarray->OrderDate)     && Show($colarray->OrderDate))     echo "<div class='table-cell right-text'><p>Date</p></div>";
		if(isset($colarray->Status)        && Show($colarray->Status))        echo "<div class='table-cell right-text'><p>Status</p></div>";
		if(isset($colarray->OrderType)     && Show($colarray->OrderType))     echo "<div class='table-cell right-text'><p>Type</p></div>";
		if(isset($colarray->JFCharName)    && Show($colarray->JFCharName))    echo "<div class='table-cell right-text'><p>Delivery By</p></div>";
		if(isset($colarray->Destination)   && Show($colarray->Destination))   echo "<div class='table-cell right-text'><p>Destination</p></div>";
		if(isset($colarray->Volume)        && Show($colarray->Volume))        echo "<div class='table-cell right-text'><p>Volume</p></div>";
		if(isset($colarray->Price)         && Show($colarray->Price))         echo "<div class='table-cell right-text'><p>Price</p></div>";
		if(isset($colarray->CancelOrder)   && Show($colarray->CancelOrder))   echo "<div class='table-cell'></div>";
		if(isset($colarray->ViewOrder)     && Show($colarray->ViewOrder))     echo "<div class='table-cell'></div>";
		if(isset($colarray->Claim)         && Show($colarray->Claim))         echo "<div class='table-cell'></div>";
		if(isset($colarray->Unclaim)       && Show($colarray->Unclaim))       echo "<div class='table-cell'></div>";
		if(isset($colarray->Deliver)       && Show($colarray->Deliver))       echo "<div class='table-cell'></div>";
		if(isset($colarray->Reorder)       && Show($colarray->Reorder))       echo "<div class='table-cell'></div>";
		echo "</div>";
		
		foreach($orderlist as $order)
		{
			echo "<div class='table-row exact-height-row'>";
			if(isset($colarray->Comments)      && Show($colarray->Comments))                 echo "<div class='table-cell'>" . (($order->Comments != null)?"<img src='images/comment.png' title='" . htmlspecialchars($order->Comments, ENT_QUOTES | ENT_HTML401) . "'/>":"") . "</div>";
			if(isset($colarray->OrderID)       && Show($colarray->OrderID))                  echo "<div class='table-cell'><p>#" . $order->OrderID . "</p></div>";
			if(isset($colarray->CharacterName) && Show($colarray->CharacterName))            echo "<div class='table-cell right-text'><p>" . htmlspecialchars($order->CharacterName, ENT_QUOTES | ENT_HTML401) . "</p></div>";
			if(isset($colarray->OrderDate)     && Show($colarray->OrderDate))                echo "<div class='table-cell right-text'><p>" . $order->OrderDate . "</p></div>";
			if(isset($colarray->Status)        && Show($colarray->Status))                   echo "<div class='table-cell right-text'><p>" . ($order->Cancelled == 1? "Cancelled" : ($order->Completed == 1? "Completed" :($order->Submitted == 1? "Submitted":"Not Submitted"))) . "</p></div>";
			if(isset($colarray->OrderType)     && Show($colarray->OrderType))                echo "<div class='table-cell right-text'><p>" . $order->OrderType . "</p></div>";
			if(isset($colarray->JFCharName)    && Show($colarray->JFCharName))               echo "<div class='table-cell right-text'><p>" . htmlspecialchars($order->JFCharName, ENT_QUOTES | ENT_HTML401) . "</p></div>";
			if(isset($colarray->Destination)   && Show($colarray->Destination))              echo "<div class='table-cell right-text'><p>" . $order->Destination . "</p></div>";
			if(isset($colarray->Volume)        && Show($colarray->Volume))                   echo "<div class='table-cell right-text'><p>" . htmlspecialchars(number_format(floatval($order->Volume), 2), ENT_QUOTES | ENT_HTML401) . " m&#179;</p></div>";
			if(isset($colarray->Price)         && Show($colarray->Price))                    echo "<div class='table-cell right-text'><p>" . htmlspecialchars(number_format(floatval($order->Price), 2), ENT_QUOTES | ENT_HTML401) . " ISK</p></div>";
			if(isset($colarray->CancelOrder)   && Show($colarray->CancelOrder))
			{
				if($order->Submitted == 1 && $order->Cancelled == 0 && $order->Completed == 0) echo "<div class='table-cell'><form action='myorders.php' method='post'><input type='hidden' name='orderid' value='" . $order->OrderID . "'/><input type='hidden' name='action' value='cancel' /><input type='submit' value='Cancel Order' onclick='return confirm(\"Confirm cancellation?\")' /></form></div>";
				else                                                                           echo "<div class='table-cell'></div>";
			}
			if(isset($colarray->ViewOrder)     && Show($colarray->ViewOrder))                echo "<div class='table-cell'><form action='vieworder.php' method='get'><input type='hidden' name='orderid' value='" . $order->OrderID . "'/><input type='submit' value='View' /></form></div>";
			if(isset($colarray->Claim)         && Show($colarray->Claim))                    echo "<div class='table-cell'><form action='freighters.php' method='post'><input type='hidden' name='orderid' value='" . $order->OrderID . "'/><input type='hidden' name='action' value='claim' /><input type='submit' value='Claim' /></form></div>";
			if(isset($colarray->Unclaim)       && Show($colarray->Unclaim))                  echo "<div class='table-cell'><form action='freighters.php' method='post'><input type='hidden' name='orderid' value='" . $order->OrderID . "'/><input type='hidden' name='action' value='unclaim' /><input type='submit' value='Unclaim' /></form></div>";
			if(isset($colarray->Deliver)       && Show($colarray->Deliver))                  echo "<div class='table-cell'><form action='freighters.php' method='post'><input type='hidden' name='orderid' value='" . $order->OrderID . "'/><input type='hidden' name='action' value='deliver' /><input type='submit' value='Deliver' onclick='return confirm(\"Confirm delivery?\")' /></form></div>";
			if(isset($colarray->Reorder)       && Show($colarray->Reorder))                  echo "<div class='table-cell'><form action='index.php' method='post'><input type='hidden' name='orderid' value='" . $order->OrderID . "'/><input type='hidden' name='action' value='reorder' /><input type='submit' value='Re Order'/></form></div>";
			echo "</div>";
		}
	}

//========== MULTI ORDER FUNCTIONS ==========    
//========== ORDER MODIFICATION FUNCTIONS ==========
	function AddToOrder($typeid, $quantity)
	{
		$orderID = GetCurrentOrderId();

		if(! IsValidTypeID($typeid)) return false;

		$selectStatement = "SELECT * FROM OrderItems WHERE OrderID = " . $orderID . " AND TypeID = " . $typeid;
		$insertStatement = "INSERT INTO OrderItems (TypeID, Quantity, OrderID) VALUES ( " . $typeid . "," . $quantity . "," . $orderID . ")";
		$updateStatement = "UPDATE OrderItems SET Quantity = Quantity + " . $quantity . " WHERE OrderID = " . $orderID . " AND TypeID = " . $typeid;
		
		$con = connect("write");
		if(! $con) throw new Exception();
		
		$result = mysqli_query($con, $selectStatement);
		if( mysqli_num_rows( $result) == 0)
		{
			mysqli_free_result($result);
			$result = mysqli_query($con, $insertStatement);
		}
		else if(mysqli_num_rows($result) == 1)
		{
			mysqli_free_result($result);
			$result = mysqli_query($con, $updateStatement);
		}
		else
		{
			mysqli_free_result($result);
			mysqli_close($con);
			throw new Exception();
		}
		mysqli_close($con);
	}

	function RemoveItem($typeid)
	{
		$orderID = GetCurrentOrderId();
		
		if(!IsValidTypeID($typeid)) return false;

		$deleteString = "DELETE FROM OrderItems WHERE OrderID = " . $orderID . " AND TypeID = " . $typeid;
		
		$con = connect("write");
		if(! $con) return false;
		
		$result = mysqli_query($con, $deleteString);
		mysqli_close($con);
		return $result;
	}

	function Submit($ordertype, $dest, $comments)
	{
		$orderid = GetCurrentOrderId();
		$itemcount = GetItemCount($orderid);
		$results = false;
		
		if($itemcount != 0)
		{
			$con = connect("write");
			if(!$con) throw new Exception();
			$com;
			if($comments == null)
			{
				$com = "NULL";
			}
			else
			{
				if(strlen($comments) > 2048) $comments = substr($comments, 0, 2048);
				$com = "'" . mysqli_real_escape_string($con, $comments) . "'";
			}
			$selectQuery = "SELECT COUNT(*) AS num FROM ServicedLocations WHERE SystemName = '" . $dest . "'";
			$updateQuery = "UPDATE Orders SET Submitted = 1, OrderDate = current_timestamp, Destination = '" . mysqli_real_escape_string($con, $dest) . "', OrderType = '" . mysqli_real_escape_string($con, $ordertype) . "', Comments = " . $com . " WHERE OrderID = " . $orderid . " AND CharacterID = " . $_SESSION['characterID'] . " AND Cancelled = 0 AND Submitted = 0";

			$results = mysqli_query($con, $selectQuery);
			$row = mysqli_fetch_array($results);

			if($row["num"] == 0)
			{
				mysqli_close($con);
				return false;
			}
			
			$results = mysqli_query($con, $updateQuery);
			mysqli_close($con);
		}
		return $results;
	}	

	function Claim($orderid)
	{
		$query = "UPDATE Orders SET JFCharacterID = " . $_SESSION['characterID'] . " WHERE OrderID = " . $orderid . " AND JFCharacterID IS NULL AND Submitted = 1 AND Cancelled = 0 AND Completed = 0";
		$con = connect("write");
		if($con)
		{
			$results = mysqli_query($con, $query);
			mysqli_close($con);
			return $results;
		}
		return false;
	}
	
	function Unclaim($orderid)
	{
		$query = "UPDATE Orders SET JFCharacterID = NULL WHERE OrderID = " . $orderid . " AND JFCharacterID = " . $_SESSION['characterID'] . " AND Submitted = 1 AND Cancelled = 0 AND Completed = 0";
		$con = connect("write");
		if($con)
		{
			$results = mysqli_query($con, $query);
			mysqli_close($con);
			return $results;
		}
		return false;
	}
	
	function Complete($orderid)
	{
		$query = "UPDATE Orders SET Completed = 1 WHERE OrderID = " . $orderid . " AND JFCharacterID = " . $_SESSION['characterID'] . " AND Submitted = 1 AND Cancelled = 0";
		$con = connect("write");
		if($con)
		{
			$results = mysqli_query($con, $query);
			mysqli_close($con);
			return $results;
		}
		return false;
	}
	
	function Cancel($orderid)
	{
		$query = "UPDATE Orders SET Cancelled = 1 WHERE OrderID = " . $orderid . " AND CharacterID = " . $_SESSION['characterID'] . " AND Submitted = 1 AND Completed = 0";
		$con = connect("write");
		if($con)
		{
			$results = mysqli_query($con, $query);
			mysqli_close($con);
			return $results;
		}
		return false;
	}
	
//=========== END OF ORDER MODIFICATION FUNCTIONS ============
//=========== COURIER MODIFICATION FUNCTIONS =============

	function CreateCourier($volume, $source, $dest, $comments)
	{
		$con = connect("write");
		if(! $con) return false;
		
			$com;
			if($comments == null)
			{
				$com = "NULL";
			}
			else
			{
				if(strlen($comments) > 2048) $comments = substr($comments, 0, 2048);
				$com = "'" . mysqli_real_escape_string($con, $comments) . "'";
			}
			if(strlen($source) > 128) $source = substr($source, 0, 128);
			if(strlen($dest) > 128) $dest = substr($dest, 0, 128);
		
		$querystring = "INSERT INTO Couriers (CharacterID, Volume, Source, Destination, CourierDate, Comments) VALUES (" . $_SESSION['characterID'] . ", " . $volume . ",'" . mysqli_real_escape_string($con, $source) . "','" . mysqli_real_escape_string($con, $dest) . "', current_date, " . $com . ")";
		
		$results = mysqli_query($con, $querystring);
		mysqli_close($con);
		return $results;
	}

	function CancelCourier($courierid)
	{
		$querystring = "UPDATE Couriers SET Cancelled = 1, CompletionDate = current_date WHERE CharacterID = " . $_SESSION['characterID'] . " AND Completed = 0 AND CourierID = " . $courierid;
		
		$con = connect("write");
		if(! $con) return false;
		$results = mysqli_query($con, $querystring);
		mysqli_close($con);
		return $results;
	}
	
	function CompleteCourier($courierid)
	{
		$querystring = "UPDATE Couriers SET Completed = 1, CompletionDate = current_date, JFCharacterID = " . $_SESSION['characterID'] . " WHERE Completed = 0 AND JFCharacterID IS NULL AND CourierID = " . $courierid;

		$con = connect("write");
		if(! $con) return false;
		$results = mysqli_query($con, $querystring);
		mysqli_close($con);
		return $results;
	}
	
	function GetCouriers($filter)
	{
		$query = "SELECT Couriers.*, Pilots.CharacterName AS CharacterName, JFChars.CharacterName AS JFCharacterName FROM (Couriers INNER JOIN Pilots ON Couriers.CharacterID = Pilots.CharacterID) LEFT JOIN Pilots AS JFChars ON Couriers.JFCharacterID = JFChars.CharacterID WHERE TRUE";
		if(isset($filter->CourierID))     $query .= " AND CourierID = " . $filter->CourierID;
		if(isset($filter->CharacterID))   $query .= " AND Couriers.CharacterID = " . $filter->CharacterID;
		if(isset($filter->JFCharacterID)) $query .= " AND JFCharacterID = " . $filter->JFCharacterID;
		if(isset($filter->Completed))     $query .= " AND Completed = " . (($filter->Completed)? "1" : "0");
		if(isset($filter->Cancelled))     $query .= " AND Cancelled = " . (($filter->Cancelled)? "1" : "0");
		$query .= " ORDER BY CASE WHEN CompletionDate IS NULL THEN 0 ELSE 1 END, CourierID DESC";
		if(isset($filter->Limit)) $query .= " LIMIT " . $filter->Limit;
		
		$con = connect("read");
		if(! $con) return false;
		$results = mysqli_query( $con, $query);
		if(! $results) return false;
		
		$counter = 0;
		$retval = Array();
		while( $row = mysqli_fetch_array($results) )
		{
			$retval[$counter] = new stdClass();
			$retval[$counter]->CourierID       = $row['CourierID'];
			$retval[$counter]->CharacterName   = $row['CharacterName'];
			$retval[$counter]->Volume          = $row['Volume'];
			$retval[$counter]->Source          = $row['Source'];
			$retval[$counter]->Destination     = $row['Destination'];
			$retval[$counter]->JFCharacterID   = $row['JFCharacterID'];
			$retval[$counter]->JFCharacterName = $row['JFCharacterName'];
			$retval[$counter]->Comments        = $row['Comments'];
			$retval[$counter]->Completed       = $row['Completed'] == 1;
			$retval[$counter]->Cancelled       = $row['Cancelled'] == 1;
			$retval[$counter]->CourierDate     = $row['CourierDate'];
			$retval[$counter]->CompletionDate  = $row['CompletionDate'];
			$counter++;
		}
		mysqli_free_result($results);
		mysqli_close($con);
		
		return $retval;
	}
	
	function OutputCouriers($couriers, $filter)
	{
		echo "<div class='table-row table-header'>";
		if(isset($filter->Comments)        && $filter->Comments)        echo "<div class='table-cell'></div>";
		if(isset($filter->CourierID)       && $filter->CourierID)       echo "<div class='table-cell' style='width:100%;'><p>Courier ID</p></div>";
		if(isset($filter->CharacterName)   && $filter->CharacterName)   echo "<div class='table-cell right-text'><p>Placed By</p></div>";
		if(isset($filter->CourierDate)     && $filter->CourierDate)     echo "<div class='table-cell right-text'><p>Date</p></div>";
		if(isset($filter->Status)          && $filter->Status)          echo "<div class='table-cell right-text'><p>Status</p></div>";
		if(isset($filter->JFCharacterName) && $filter->JFCharacterName) echo "<div class='table-cell right-text'><p>Delivered By</p></div>";
		if(isset($filter->Volume)          && $filter->Volume)          echo "<div class='table-cell right-text'><p>Volume (m&#179;)</p></div>";
		if(isset($filter->Source)          && $filter->Source)          echo "<div class='table-cell right-text'><p>Source</p></div>";
		if(isset($filter->Destination)     && $filter->Destination)     echo "<div class='table-cell right-text'><p>Destination</p></div>";
		if(isset($filter->Complete)        && $filter->Complete)        echo "<div class='table-cell'></div>";
		if(isset($filter->Cancel)          && $filter->Cancel)          echo "<div class='table-cell'></div>";
		if(isset($filter->Undo)            && $filter->Undo)            echo "<div class='table-cell'></div>";
		echo "</div>";
		
		foreach($couriers as $courier)
		{
			echo "<div class='table-row exact-height-row'>";
			if(isset($filter->Comments)        && $filter->Comments)        echo "<div class='table-cell'>" . (($courier->Comments != null) ? "<img src='images/comment.png' title='" . htmlspecialchars($courier->Comments, ENT_QUOTES | ENT_HTML401) . "' />" : "") . "</div>";
			if(isset($filter->CourierID)       && $filter->CourierID)       echo "<div class='table-cell'><p>#" . $courier->CourierID . "</p></div>";
			if(isset($filter->CharacterName)   && $filter->CharacterName)   echo "<div class='table-cell right-text'><p>" . htmlspecialchars($courier->CharacterName, ENT_QUOTES | ENT_HTML401) . "</p></div>";
			if(isset($filter->CourierDate)     && $filter->CourierDate)     echo "<div class='table-cell right-text'><p>" . $courier->CourierDate . "</p></div>";
			if(isset($filter->Status)          && $filter->Status)          echo "<div class='table-cell right-text'><p>" . (($courier->Completed)? "Completed" : (($courier->Cancelled)? "Cancelled": "New")) . "</p></div>";
		  if(isset($filter->JFCharacterName) && $filter->JFCharacterName) echo "<div class='table-cell right-text'><p>" . htmlspecialchars($courier->JFCharacterName, ENT_QUOTES | ENT_HTML401) . "</p></div>";
			if(isset($filter->Volume)          && $filter->Volume)          echo "<div class='table-cell right-text'><p>" . number_format(floatval($courier->Volume),2) . " m&#179;</p></div>";
			if(isset($filter->Source)          && $filter->Source)          echo "<div class='table-cell right-text'><p>" . htmlspecialchars($courier->Source, ENT_QUOTES | ENT_HTML401) . "</p></div>";
			if(isset($filter->Destination)     && $filter->Destination)     echo "<div class='table-cell right-text'><p>" . htmlspecialchars($courier->Destination, ENT_QUOTES | ENT_HTML401) . "</p></div>";
			if(isset($filter->Complete)        && $filter->Complete)        echo "<div class='table-cell'>" . ((! $courier->Completed && ! $courier->Cancelled)?"<form action='freighters.php' method='post'><input type='hidden' name='action' value='finish' /><input type='hidden' name='courierid' value='" . $courier->CourierID . "' /><input type='submit' value='Finish Courier' onclick='return confirm(\"Confirm delivery?\")'/></form>":"") . "</div>";
			if(isset($filter->Cancel)          && $filter->Cancel)          echo "<div class='table-cell'>" . ((! $courier->Completed && ! $courier->Cancelled)?"<form action='myorders.php' method='post'><input type='hidden' name='action' value='delete' /><input type='hidden' name='courierid' value='" . $courier->CourierID . "' /><input type='submit' value='Cancel' onclick='return confirm(\"Confirm cancellation?\")' /></form>" : "" ) . "</div>";
			if(isset($filter->Undo)            && $filter->Undo)            echo "<div class='table-cell'>" . (($courier->JFCharacterID == $_SESSION['characterID'])?"<form action='myorders.php' method='post'><input type='hidden' name='action' value='undo' /><input type='hidden' name='courierid' value='" . $courier->CourierID . "'/><input type='submit' value='Undo Delivery' onclick='return confirm(\"Confirm undo delivery?\")' /></form>":"") . "</div>";
			echo "</div>";
		}
	}
	
	function UndoComplete($courierid)
	{
		$querystring = "UPDATE Couriers SET Completed = 0, CompletionDate = NULL, JFCharacterID = NULL WHERE CourierID = " . $courierid . " AND JFCharacterID = " . $_SESSION['characterID'] . " AND Completed = 1";
		
		$con = connect("write");
		if(! $con) return false;
		$results = mysqli_query($con, $querystring);
		mysqli_close($con);
		return $results;
	}
	
//=========== END OF COURIER MODIFICATION FUNCTIONS =============
//=========== MISC FUNCTIONS ==========
	function PopulateLocationsDropdown()
	{
		echo "<select name='dest'>";
		try
		{
			$query = "SELECT * FROM ServicedLocations";
			
			$con = connect("read");
			if(! $con) throw new Exception();//skips the enumeration part so that we can close the select tags
			
			$results = mysqli_query($con, $query);
			
			echo "<option value='none' disabled selected>Deliver To</option>";
			while($row = mysqli_fetch_array($results))
			{
				if(!empty($row))
				{
					echo "<option value='" . $row["SystemName"] . "'>" . htmlspecialchars($row["SystemName"], ENT_QUOTES | ENT_HTML401) . "</option>";
				}
			}

			
			mysqli_close($con);
		}
		catch(Exception $ex)
		{}
		echo "</select>";
	}

	function GetItemCount($orderid)
	{
		$query = "SELECT COUNT(*) AS num FROM OrderItems WHERE OrderID = " . $orderid;
		
		$con = connect("read");
		if(! $con) throw new Exception();
		
		$result = mysqli_query($con, $query);
		$arr = mysqli_fetch_array($result);
		$retval = intval($arr["num"]);
		mysqli_free_result($result);
		mysqli_close($con);

		return $retval;
	}
	
	function IsValidTypeID($typeid)
	{
		$query = "SELECT COUNT(*) AS num FROM Items WHERE TypeID = " . $typeid;

		$con = connect("read");
		if(!$con) return false;

		$result = mysqli_query($con, $query);
		$retval = false;
		if($result)
		{
			$arr = mysqli_fetch_array($result);
			$retval = intval($arr['num']) == 1;
		}
		mysqli_free_result($result);
		mysqli_close($con);
		return $retval;
	}
//=========== END MISC FUNCTIONS ==========
?>
