<?php
	include("database.php");
	$error_msg = "";

	function error()
	{
		global $error_msg;
		return $error_msg;
	}
	
	//inserts/updates the specified character in the database
	function updateInDB($characterName, $characterID, $hashedPW, $salt)
	{
		global $error_msg;
		$error_msg = "";
		
		$con = connect("write");
		if(! $con)
		{
			throw new Exception();
		}
		
		$selectQuery = "SELECT COUNT(*) AS num FROM Pilots WHERE CharacterID = " . $characterID;
		$insertQuery = "INSERT INTO Pilots ( CharacterID, CharacterName, HashedPW, Salt ) VALUES(" . $characterID . ",'" . mysqli_real_escape_string($con, $characterName) . "','" . $hashedPW . "','" . $salt . "')";
		$updateQuery = "UPDATE Pilots SET HashedPW = '" . $hashedPW . "', Salt = '" . $salt . "' WHERE CharacterID = " . $characterID;
		
		$results = mysqli_query($con, $selectQuery);
		
		if(! $results )
		{
			$error_msg = "Error: 0x1: There was an error registering this character. Please contact an administrator.";
			mysqli_close($con);
			return false;
		}
		$arr = mysqli_fetch_array($results);
		mysqli_free_result($results);
		$count = intval($arr["num"]);
		
		if($count == 0)
		{
			//we need to insert a new character
			if(!mysqli_query($con, $insertQuery))
			{
				$error_msg = "Error: 0x2: There was an error registering this character. Please contact an administrator";
			}
		}
		else if($count == 1)
		{
			if(!mysqli_query($con, $updateQuery))
			{
				echo $updateQuery;
				$error_msg = "Error: 0x3: There was an error registering this character. Please contact an administrator";
			}
		}
		else $error_msg = "Error: 0x4: There was an error registering this character. Please contact an administrator.";
		mysqli_close($con);
		
		return $error_msg == "";
	} //end register
	
	//Checks if the apikey is valid and corresponds to the required character
	function isApiKeyValid($keyID, $vCode, $characterID)
	{
		global $error_msg;
		$error_msg = "";
		if( empty($keyID) || ! ctype_digit($keyID))
		{
			$error_msg = "Error: The provided keyID " . $keyID . " is not valid.";
			return false;
		}
		if( empty($vCode) || ! ctype_alnum($vCode))
		{
			$error_msg = "Error: The provided vCode " . $vCode . " is not valid.";
			return false;
		}
		if( empty($characterID))
		{
			$error_msg = "Error: The provided character id is invalid.";
			return false;
		}
		if(! is_int($characterID))
		{
			if(ctype_digit($characterID))
			{
				$characterID = intval($characterID);
			}
			else
			{
				$error_msg = "Error: The provided character id was not an integer";
				return false;
			}
		}
		
		$queryUrl = "https://api.eveonline.com/account/Characters.xml.aspx?keyID=" . $keyID . "&vCode=" . $vCode;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $queryUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		
		$returnedXML = curl_exec($ch);
		$success = false;
		if($returnedXML)
		{
			$xml = new SimpleXMLElement($returnedXML);
			if(isset($xml->result))
			{
				foreach($xml->result->rowset->row as $row)
				{
					$success |= (intval($row["characterID"]) == $characterID);
				}
				if(!$success) $error_msg = "Error: The specified character was not on the account.";
			}
			else
			{
				$error_msg = "Error: The specified api key does not have access to the characters data.";
			}
		}
		curl_close($ch);
		return $success;
	}

	//gets the character id for a provided name
	function getCharacterID($characterName)
	{
		global $error_msg;
		$error_msg = "";
		if($characterName == null || $characterName == "")
		{
			$error_msg = "Error: Invalid character name provided.";
			return false;
		}
		
		$charIDQuery = "https://api.eveonline.com/eve/CharacterID.xml.aspx?names=" . urlencode($characterName);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $charIDQuery);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		
		$returnedXML = curl_exec($ch);
		if($returnedXML)
		{
			$xml = new SimpleXMLElement($returnedXML);
			if(! isset($xml->result))
			{
				$error_msg = "Error: Provided character name could not be found.";
				curl_close($ch);
				return false;
			}
			$characterID = intval($xml->result->rowset->row["characterID"]);
			curl_close($ch);
			if($characterID == 0)
			{
				$error_msg = "Error: Provided character name could not be found.";
				return false;
			}
			return $characterID;
		}
		$error_msg = "Error: Could not query eve api for character name.";
		return false;
	}

	//returns empty string if no alliance or an issue
	function getAllianceID($characterID)
	{
		global $error_msg;
		$error_msg = "";
		if(empty($characterID) || ! ctype_digit($characterID))
		{
			$error_msg = "Error: Invalid character id provided.";
			return "";
		}
		
		$characterInfoQuery = "https://api.eveonline.com/eve/CharacterInfo.xml.aspx?characterID=" . $characterID;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $characterInfoQuery);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		$allianceID = "";
		
		$returnedXML = curl_exec($ch);
		if($returnedXML)
		{
			$xml = new SimpleXMLElement($returnedXML);
			if(isset($xml->result))
			{
				if($xml->result->allianceID != null)
				{
					$allianceID = $xml->result->allianceID . ""; //use the string because otherwise it returns a simplexmlelement
				}
				else $error_msg = "Error: Provided character is not in an alliance.";
			}
			else $error_msg = "Error: Eve api server returned a value other than expected.";
		}
		else $error_msg = "Error: Eve api server returned no value, the server may be down.";
		curl_close($ch);
		return $allianceID;
	}
	
	//returns true or false
	//if false check the error(), if error is empty then they are not allowed, if it has contents then there was an error
	function isCharInAllowedAliance($allianceID)
	{
		global $error_msg;
		$error_msg = "";
		if( empty($allianceID) || (!is_int($allianceID) && ! ctype_digit($allianceID)))
		{
			$error_msg = "Error: Invalid alliance id provided";
			return false;
		}
		$query = "SELECT COUNT(*) AS num FROM AllowedAlliances WHERE AllianceID = " . $allianceID;
		$con = connect("read");
		if($con)
		{
			$results = mysqli_query($con, $query);
			$arr = mysqli_fetch_array($results);
			mysqli_free_result($results);
			if(intval($arr["num"]) == 1)
			{
				mysqli_close($con);
				return true;
			}
			mysqli_close($con);
			return false;
		}
		else throw new Exception();
		$error_msg = "Error: could not connect to database.";
		return false;
	}
	
	//registers a character in the database if valid.
	function register($characterName, $keyID, $vCode, $password)
	{
		global $error_msg;

		$characterID = getCharacterID($characterName);
		if( $characterID == false)
		{
			return false;
		}
		
		$keyValid = isApiKeyValid($keyID, $vCode, $characterID);
		if( ! $keyValid )
		{
			return false;
		}
		
		$allianceID = getAllianceID($characterID);
		if( $allianceID == "" )
		{
			return false;
		}
		
		$isCharAllowed = isCharInAllowedAliance($allianceID);
		if(! $isCharAllowed )
		{
			$err = error();
			if($err == "")
			{
				$error_msg =  "Character is not in an alliance that is allowed to register.";
			}
			return false;
		}
		//generate a salt for them here
		$salt = base64_encode(mcrypt_create_iv(256, MCRYPT_RAND));
		if(! $salt)
		{
			return false;
		}
		//hash their password
		$passwordHash = crypt( $password, $salt);
		$successfulDBUpdate = updateInDB($characterName, $characterID, $passwordHash, $salt);
		if(! $successfulDBUpdate)
		{
			return false;
		}
		return true;
	}

	function slowEquals($a, $b)
	{
		$diff = strlen($a) ^ strlen($b);
		for($i = 0; $i < strlen($a) && $i < strlen($b); $i++)
		{
			$diff |= ord($a[$i]) ^ ord($b[$i]);
		}
		return $diff === 0;
	}
	
	function isCharStillInValidAlliance($characterID)
	{
		$allianceID = getAllianceID($characterID);
		$result = isCharInAllowedAliance($allianceID);
		return $result;
	}
	
	//returns a character id if valid, false otherwise
	function isLoginValid($characterName, $password)
	{
		global $error_msg;
		$error_msg = "";

		$con = connect("read");
		if(!$con)
		{
			throw new Exception();
		}
		
		$selectStatement = "SELECT CharacterID, HashedPW, Salt FROM Pilots WHERE CharacterName = '" . mysqli_real_escape_string($con, $characterName) . "'";
		$results = mysqli_query($con, $selectStatement);
		$retval = false;
		
		$error_msg = "Error: The character was not found in the database. Please consider registering it.";
		if( mysqli_num_rows($results) == 1 )
		{
			$error_msg = "Error: Login failed.";
			$row = mysqli_fetch_array($results);
			
			if(! isCharStillInValidAlliance($row["CharacterID"]))
			{
				$error_msg = "Error: Character is not in an alliance that has access to this service.";
			}
			else
			{
				$hashedpw = crypt( $password, $row["Salt"]);
				$retval = slowEquals($hashedpw, $row["HashedPW"]);
				if($retval)
				{
					$error_msg = "";
					$retval = $row["CharacterID"];
				}
			}
		}
		mysqli_free_result($results);
		mysqli_close($con);
		return $retval;
	}
?>