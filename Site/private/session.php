<?php
	if(! defined("_SESSION_PHP__"))
	{
		session_start();
		define("__SESSION_PHP__","/mnt/www");
		include("database.php");
		function isLoggedIn()
		{

			return isset($_SESSION['characterID']);
		}
		
		function login($charID)
		{
			if(! empty($charID))
			{
				$_SESSION['characterID'] = $charID;
			}
		}
		
		function isJfPilot()
		{
			if(isLoggedIn())
			{
				$query = "SELECT IsJfPilot FROM Pilots WHERE CharacterID = " . $_SESSION['characterID'];
				$con = connect("read");
				if($con)
				{
					$results = mysqli_query($con, $query);
					$isjf = false;
					if(mysqli_num_rows($results) == 1)
					{
						$row = mysqli_fetch_array($results);
						$isjf = $row['IsJfPilot'];
					}
					mysqli_free_result($results);
					mysqli_close($con);
					return $isjf;
				}else throw new Exception();
			}
			return false;
		}
		
		function logout()
		{
			if(isset($_SESSION['characterID'])) unset($_SESSION['characterID']);
			session_destroy();
		}
	}
?>
