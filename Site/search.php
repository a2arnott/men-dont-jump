<?php
	include("private/logincheck.php");
	include("private/general.php");
	include("private/database.php");
	
	$resultArray = false;
	$searchmsg = '';
	
	//====================================== section for processing a query
	if($_SERVER["REQUEST_METHOD"] == "GET")
	{
		$searchstring = input($_GET['search']);
		$page = (isset($_GET['p']) && ctype_digit($_GET['p']))?intval(input($_GET['p'])):0;
		$resultcount = 0;
		if($searchstring != false)
		{
			try
			{
				$con = connect("read");
				if($con)
				{
					$str = mysqli_real_escape_string($con, $searchstring);
					$querystring = "SELECT * FROM Items WHERE ItemName LIKE '%" . $str . "%' ORDER BY CASE WHEN ItemName LIKE '" . $str . " %' THEN 0 WHEN ItemName LIKE '" . $str . "%' THEN 1 WHEN ItemName LIKE '% " . $str . "%' THEN 2 ELSE 3 END, ItemName LIMIT " . 10 * $page . "," . 10;
					
					$results = mysqli_query($con, $querystring);
					if($results)
					{
						$i = 0;
						while($resultArray[$i] = mysqli_fetch_array($results)) $i++;
					}
					else $searchmsg = "No results returned for your query.";
					mysqli_free_result($results);
					
					$querystring = "SELECT COUNT(*) AS NUM FROM Items WHERE ItemName LIKE '%" . $str . "%'";
					$results = mysqli_query($con, $querystring);
					if($results)
					{
						$row = mysqli_fetch_array($results);
						$resultcount = intval($row['NUM']);
					}
					
					mysqli_free_result($results);
					mysqli_close($con);
				}
				else $searchmsg = "There was an internal server error while performing your query.";
			}
			catch(Exception $ex)
			{
				$searchmsg = "There was an internal server error while performing your query.";
			}
		}
		else $searchmsg = "No search parameter specified.";
	}
	
	function PrevNextBanner($resultcount, $page, $searchstring)
	{
		if($resultcount > 10)
		{
			echo "<div class='inner item'><div class='table'><div class='table-row'><div class='table-cell'>";
			if($page > 0)
			{
				echo "<a href='search.php?search=";
				echo urlencode($searchstring);
				echo "&p=";
				echo $page - 1;
				echo "'><p>Previous</p></a>";//show left buttons
			}
			echo "</div><div class='table-cell'  style='width:100%;'></div><div class='table-cell'>";
			if(($page+1)*10 < $resultcount)
			{
				echo "<a href='search.php?search=";
				echo urlencode($searchstring);
				echo "&p=";
				echo $page + 1;
				echo "'><p>Next</p></a>";//show right buttons
			}
			echo "</div></div></div></div>";
		}				
	}
	//========================================= end of section for processing a query

?>
<!DOCTYPE html>
<html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/general.css" />
		<title>Searching - <?php echo $searchstring; ?></title>
		<link rel="shortcut icon" href="images/favicon.ico" />
	</head>
	<body>
		<div class='gradient'>
			<?php include("private/header.php"); ?>
			<div class="outer">
				<div class="middle">
					<?php
						if(isset($_GET['status']) && $_GET['status'] == 'invalid')
						{
							echo "<div class='inner'><p>The quantity specified is invalid.</p></div>";
						}
						PrevNextBanner($resultcount, $page, $searchstring);
					
						if($searchmsg == '')
						{
							$i = 0;
							foreach($resultArray as $item)
							{
								if(! empty($item["ItemName"]))
								{
									echo '<div class="inner item"><div class="table"><div class="table-row">';
									echo '<div class="table-cell"><img src="https://image.eveonline.com/Type/' . $item["TypeID"] . '_32.png" onclick="CCPEVE.showInfo(' . $item['TypeID'] . ')" class="clickable" style="margin: -5px; float:left;"/></div>';
									echo '<div class="table-cell clickable" style="width: 100%;" onclick="CCPEVE.showInfo(' . $item['TypeID'] . ')"><p>' . htmlspecialchars($item["ItemName"]) . "</p></div>";
									echo '<div class="table-cell"><form action="index.php" method="post"><input type="hidden" name="ref" value="' . urlencode("search.php?search=" . $searchstring . "&p=" . $page ) . '"/><input type="hidden" name="action" value="add" />';
									echo number_format(floatval($item["Price"]),2) . ' ISK ';
									echo '<input type="number" name="qty" placeholder="Quantity" /><input type="hidden" name="typeID" value="' . $item["TypeID"] . '"/>';
									echo '<input type="submit" value="Add to Cart"/>';
									echo '</form></div></div>';
									echo '</div></div>';
									$i++;
								}
							}
							if($i == 0) echo "<div class='inner'><p>No results found for the specified search term.</p></div>";
						}
						else
						{
							echo "<div class='inner'><p>" . $searchmsg . "</p></div>";
						}
						
						PrevNextBanner($resultcount, $page, $searchstring);
					?>
				</div>
			</div>
		</div>
	</body>
</html>