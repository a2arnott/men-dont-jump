<?php
	include("private/logincheck.php");
	include("private/orderfns.php");
	include("private/general.php");
	
	//========================================= section for loading the current order details
	$ordermsg = '';
	$order = NULL;
	try
	{
		$orderid = GetCurrentOrderId();
		$order = GetOrderDetails($orderid);
	}
	catch(Exception $ex)
	{
		$ordermsg = "An error occurred fetching the current order.";
	}
	//========================================= end of section for current order details
	//========================================= section for addingto /removing from current order
	$updatemsg = '';
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$action    = isset($_POST['action'])                                                                      ?input($_POST['action']):false;
		$typeid    =(isset($_POST['typeID']) && ctype_digit($_POST['typeID']))                                    ?input($_POST['typeID']):false;
		$ordertype = isset($_POST['orderType'])                                                                   ?input($_POST['orderType']):false;
		$quantity  =(isset($_POST['qty']) && ctype_digit($_POST['qty']) && intval($_POST['qty']) > 0)             ?input($_POST['qty']):false;
		$dest      = isset($_POST['dest'])                                                                        ?input($_POST['dest']):false;
		$comments  = isset($_POST['comments'])                                                                    ?input($_POST['comments']):null;
		$reorderid =(isset($_POST['orderid']) && ctype_digit($_POST['orderid']) && intval($_POST['orderid']) > 0) ?input($_POST['orderid']):null;
		
		if($action == false || ($action != 'add' && $action != 'remove' && $action != 'submit' && $action != 'reorder')) $updatemsg = "There was an error processing the request.";
		else if(($action == 'add' || $action == 'remove') && ($typeid == false || ! ctype_digit($typeid)))               $updatemsg = "There was an error with the item type id passed to the server.";
		else if($action == 'submit' && ($ordertype == false || ($ordertype != 'personal' && $ordertype != 'market')))    $updatemsg = "No order type was specified.";
		else if($action == 'reorder' && ($reorderid == null || ! ctype_digit($reorderid)))                               $updatemsg = "Failed to re-add items to the current order.";
		else if($action == 'add' && ($quantity == false || ! ctype_digit($quantity)))
		{
			if(isset($_POST['ref']))
			{
				header("Location: " . urldecode($_POST['ref']) . "&status=invalid");
				exit;
			}
			$updatemsg = "The quantity specified is not valid.";
		}
		else if($action == 'submit' && $dest == false )                                                               $updatemsg = "No destination specified.";
		else
		{
			try
			{
				if($action == 'add')
				{
					AddToOrder($typeid, $quantity);
					header("Location: /"); //we do this so on refresh the user doesn't get prompted to resend data
					exit;
				}
				else if($action == 'remove')
				{
					RemoveItem($typeid);
					header("Location: /");
					exit;
				}
				else if($action == 'submit')
				{
					if(Submit($ordertype, $dest, $comments))
					{
						header("Location: /?status=success");
						exit;
					}
					else
					{
						$updatemsg = "The specified destination is invalid.";
					}
				}
				else if($action == 'reorder')
				{
					Reorder($reorderid);
					header("Location: /");
					exit;
				}
				else $updatemsg = "The requested action is unknown";
			}
			catch(Exception $ex)
			{
				$updatemsg = "There was an internal server error while trying to process your request.";
			}
		}
	}
	else if($_SERVER['REQUEST_METHOD'] == 'GET')
	{
		$success = (isset($_GET['status']) && $_GET['status'] == 'success');
	}
	//========================================= end of section for modifying current order
?>
<!DOCTYPE html>
<html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/general.css" />
		<title>Shopping Cart</title>
		<link rel="shortcut icon" href="images/favicon.ico" />
	</head>
	<body>
		<div class='gradient'>
			<?php include("private/header.php"); ?>
			<div class="outer">
				<div class="middle">
					<div class='inner'>
						<form action="search.php" method="get">
							<input type="text" name="search" placeholder="Search for an Item" />
							<input type="submit" value="Search" />
						</form>
					</div>
					<?php
						if($updatemsg != '')
						{
							echo "<div class='inner'><p>" . $updatemsg . "</p></div>";
						}
						if(isset($success) && $success)
						{
							echo "<div class='inner'><p>Success! Your order has been submitted and is awaiting a freighter pilot to accept it for delivery.</p></div>";
						}
					?>
					<div class="inner"> <!-- currentorder -->
						<?php
							if($ordermsg == "")
							{
								$order = GetOrderDetails($orderid);
								if($order != NULL)
								{
									$colarray = new stdClass();
									$colarray->TitleText = "Shopping Cart";
									$colarray->Submit  = (count($order->Items) > 0);
									OutputOrderTitle($order, $colarray);
									unset($colarray);
									
									echo "<div class='resultsbox'>";
									$colarray = new stdClass();
									$colarray->ItemName = true;
									$colarray->Price    = true;
									$colarray->Quantity = true;
									$colarray->Remove   = true;
									echo "<div class='table'>";
									OutputOrderRows($order, $colarray);
									echo "</div>";
									unset($colarray);
									echo "</div>";
								}
								else echo "<p>You have not added anything to your order.</p>";
							}
							else echo "<p>" . $msg . "</p>";
						?>
					</div> <!-- endof current order-->
					<div class='inner'>
						<div class='resultsbox'>
							<p><u>Creating an Order</u></p>
							<p>So you want some stuff from Jita? Here are the basics of creating an order for delivery.</p>
							<p>There are 2 types of orders to know about.</p>
							<ul>
								<li><p>Give to Me: Orders that are given or contracted directly to you upon arrival.</p></li>
								<li><p>Put on Market: Orders that are put up on open market at a price of the shippers discretion.</p></li>
							</ul>
							<p>Adding items to your order is done by searching for the item using either the search box above your shopping cart, or to the left of the logout button.</p>
							<p>Once you have found the item you want, simply enter the quantity and click &quot;Add to Cart&quot; and you will now see the item added to your cart.</p>
							<p>When done shopping, select a delivery destination, an order type, and click &quot;Checkout&quot;</p>
							<p>The order is now waiting for a jumpfreighter pilot to purchase the items, and ship them.</p>
							<br/>
							<p>Note: All prices on this site are the <a href='http://forums.black-legion.us/viewtopic.php?f=28&t=916#p7681'>2.5th percentile of sell prices for The Forge region</a> and are therefore only an estimation. Jumpfreighter pilots are free to set their own prices and determine when and how they get paid, just as you are free to reject an order when it arrives if the shipper wants too much for it.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>