<?php
	include("private/general.php");
	include("private/auth.php");
	$msg = '';
	try
	{
		if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$uname = input($_POST['uName']);
			$pass = $_POST['pw'];
			$apiid = input($_POST['apiid']);
			$vCode = input($_POST['vCode']);
			
			if( $uname == false) $msg = "The specified character name is invalid";
			else if($pass == false) $msg = "The password cannot be empty.";
			else if(! ctype_digit($apiid)) $msg = "The specified API ID was invalid.";
			else if(! ctype_alnum($vCode)) $msg = "The specified vCode was invalid.";
			else if(! register($uname, $apiid, $vCode, $pass)) $msg = "The registration failed, please verify all values are correct.";
			else
			{
				header("Location: login.php");
				exit;
			}
		}
	}
	catch(Exception $ex)
	{
		$msg = "There was an internal server error trying to register.";
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/general.css" />
		<title>Register for MenDontJump</title>
		<link rel="shortcut icon" href="images/favicon.ico" />
	</head>
	<body>
		<div class='gradient'>
			<div class="outer">
				<div class="middle">
					<div class="inner">
						<?php
							if($msg != '') echo "<p>" . $msg . "</p>";
						?>
						<form name="register" action="register.php" method="post">
							<input type="text" name="uName" placeholder="Character Name" /><br/>
							<input type="password" name="pw" placeholder="Password" /><br/>
							<input type="text" name="apiid" placeholder="API ID" /><br/>
							<input type="text" name="vCode" placeholder="vCode" /><br/>
							<input type="submit" value="Register" style="width: 100%;"/>
						</form>
						<form name="createapi" action="http://community.eveonline.com/support/api-key/CreatePredefined?accessMask=0" target="_blank" method="get"/>
							<input type="submit" value="Create API Key" style="width: 100%;"/>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>