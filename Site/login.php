<?php
	include("private/general.php");
	include("private/auth.php");
	$valid = true;
	
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		//The form posts the results back to the same page so we'll process it
		$uname = input($_POST['uName']);
		$pass = $_POST['pw'];
		if($uname != false && $pass != false )
		{
			$loginvalid = isLoginValid($uname, $pass);
			if($loginvalid)
			{
				session_start();
				$_SESSION['characterID'] = $loginvalid;
				header("Location: index.php");
				exit;
			}
		}
		$valid = false;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/general.css" />
		<title>Login</title>
		<link rel="shortcut icon" href="images/favicon.ico" />
	</head>
	<body>
		<div class='gradient'>
			<div class="outer">
				<div class="middle">
					<?php
						if(!$valid)
						{
							echo "<div class='inner'><p>The specified credentials did not match any on record.</p></div>";
						}
					?>
					<div class="inner">
						<form name="login" action="login.php" method="post">
							<input type="text" name="uName" placeholder="Character Name"/><br/>
							<input type="password" name="pw" placeholder="Password" /><br/>
							<input type="submit" value="Login" style="width:100%;"/>
						</form>
						<form name="register" action="register.php" method="get">
							<input type="submit" value="Register" style="width:100%;"/>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>