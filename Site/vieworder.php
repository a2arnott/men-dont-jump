<?php
	include("private/logincheck.php");
	include("private/orderfns.php");
	include("private/general.php");
	
	if($_SERVER["REQUEST_METHOD"] != "GET")
	{
		header("Location: error.php");
		exit;
	}
	$msg = '';
	$orderid = input($_GET["orderid"]);
	if($orderid == false || !ctype_digit($orderid)) $msg = "The orderid is was not specified.";
	
	$orderDetails = GetOrderDetails($orderid);
	if($orderDetails == NULL)
	{
		header("Location: error.php");
		exit;
	}

	if($orderDetails->CharacterID == $_SESSION['characterID'] ||
	   isJfPilot() && (
	   $orderDetails->JFCharacterID == null ||
		 $orderDetails->JFCharacterID == $_SESSION['characterID'])
		 && $orderDetails->Submitted == 1); //do nothing, this is fine (too lazy to negate the expression)
	else
	{
		header("Location: index.php");
		exit;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/general.css" />
		<title>Viewing Order #<?php echo $orderid; ?></title>
		<link rel="shortcut icon" href="images/favicon.ico" />
	</head>
	<body>
		<div class='gradient'>
			<?php include("private/header.php"); ?>
			<div class="outer">
				<div class="middle">
					<div class="inner">
						<?php
							$colarray = new stdClass();
							$colarray->newlines      = true;
							$colarray->OrderID       = true;
							$colarray->CharacterName = true;
							$colarray->Destination   = true;
							$colarray->Status        = true;
							$colarray->OrderType     = true;
							$colarray->Deliver       = (intval($orderDetails->JFCharacterID) == intval($_SESSION['characterID']));
							$colarray->Buttons       = true;
							OutputOrderTitle($orderDetails, $colarray);
							unset($colarray);
						?>
						<div class="resultsbox">
							<?php
								$colarray = new stdClass();
								$colarray->ItemName  = true;
								$colarray->Price     = true;
								$colarray->Volume    = true;
								$colarray->Quantity  = true;
								$colarray->Remove    = ($orderDetails->CharacterID == $_SESSION['characterID'] && $orderDetails->Submitted == 0);
								OutputOrderRows($orderDetails, $colarray);
								unset($colarray);
							?>
						</div><!--resultsbox-->
					</div>
				</div>
			</div>
		</div>
	</body>
</html>