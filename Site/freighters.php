<?php
  include("private/logincheck.php");
  include("private/orderfns.php");
	include("private/general.php");
	include("private/database.php");

	$msg = "";
	
	if(isset($_SERVER['HTTP_EVE_SHIPTYPEID'])&& ! isJfPilot())
	{
		$shipid = intval(preg_replace("[^0-9]", "", $_SERVER['HTTP_EVE_SHIPTYPEID']));
	  
		if($shipid == 28844 || $shipid == 28846 || $shipid == 28850 || $shipid == 28848)
		{
			$querystring = "UPDATE Pilots SET IsJfPilot = 1 WHERE CharacterID = " . $_SESSION['characterID'];
			$con = connect("write");
			if($con)
			{
				$setjfresult = mysqli_query($con, $querystring);
				if(! $setjfresult ) $msg = "Failed to set you as a jumpfreighter pilot.";
				mysqli_close($con);
			}
		}
	}
	
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$action = isset($_POST['action'])?input($_POST['action']):false;
		$orderid = (isset($_POST['orderid']) && ctype_digit($_POST['orderid']))?input($_POST['orderid']):false;
		$courierid = (isset($_POST['courierid']) && ctype_digit($_POST['courierid']))?input($_POST['courierid']):false;
	
		if($action == false || ($action != 'claim' && $action != 'unclaim' && $action != 'deliver' && $action != "finish")) $msg = "There was an error performing the requested action.";
		else if(($action == 'claim' || $action == 'unclaim' || $action == 'deliver' ) && ($orderid == false || ! ctype_digit($orderid))) $msg = "The specified orderid is invalid.";
		else if($action == 'finish' && ($courierid == false || ! ctype_digit($courierid))) $msg = "The specified courier id could not be found.";
		else
		{
			if($action == 'claim')
			{
				if(Claim($orderid))
				{
					header("Location: freighters.php");
					exit;
				}
				else $msg = "There was an error attempting to claim order #" . $orderid . ".";
			}
			
			if($action == 'unclaim')
			{
				if(Unclaim($orderid))
				{
					header("Location: freighters.php");
					exit;
				}
				else $msg = "There was an error attempting to unclaim order #" . $orderid . ".";
			}
			
			if($action == 'deliver')
			{
				if(Complete($orderid))
				{
					header("Location: freighters.php");
					exit;
				}
				else $msg = "There was an error attempting to complete order #" . $orderid . ".";
			}
			
			if($action == 'finish')
			{
				if(CompleteCourier($courierid))
				{
					header("Location: freighters.php");
					exit;
				}
				else $msg = "There was an error attempting to finish courier contract #" . $courierid . ".";
			}
		}
	}
	
	$filter = new stdClass();
  $filter->JFCharacterID = $_SESSION['characterID'];
  $filter->Completed     = false;
  $filter->Cancelled     = false;
  $filter->Submitted     = true;
  $myClaimedOrders = GetListOfOrders($filter);
  unset($filter);
  
	$filter = new stdClass();
  $filter->JFCharacterID = false;
  $filter->Completed     = false;
  $filter->Cancelled     = false;
  $filter->Submitted     = true;
  $unclaimedOrders = GetListOfOrders($filter);
  unset($filter);
	
	$filter = new stdClass();
	$filter->Completed = false;
	$filter->Cancelled = false;
	$couriers = GetCouriers($filter);
	unset($filter);
?>
<!DOCTYPE html>
<html>
  <head>
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/general.css" />
		<title>Freighters</title>
		<link rel="shortcut icon" href="images/favicon.ico" />
  </head>
  <body onload='CCPEVE.requestTrust("http://mendontjump.com");CCPEVE.requestTrust("http://www.mendontjump.com")'>
		<div class='gradient'>
			<?php include("private/header.php"); ?>
			<div class="outer">
				<div class="middle">
				<?php
					if($msg != "")
					{
						echo "<div class='inner'><p>" . $msg . "</p></div>";
					}
					if(isJfPilot())
					{
				?>
					<div class="inner">
						<p>My Claimed Orders</p>
							<div class="resultsbox">
							<?php
								if(count($myClaimedOrders) == 0)
								{
									echo "<p>You have not claimed any orders for delivery.</p>";
								}
								else if(count($myClaimedOrders) > 0)
								{
									$cols = new stdClass();
									$cols->Comments      = true;
									$cols->OrderID       = true;
									$cols->CharacterName = true;
									$cols->OrderDate     = true;
									$cols->OrderType     = true;
									$cols->Destination   = true;
									$cols->Volume        = true;
									$cols->Price         = true;
									$cols->ViewOrder     = true;
									$cols->Unclaim       = true;
									$cols->Deliver       = true;
									OutputOrderList($myClaimedOrders, $cols);
									unset($cols);
								}
								else echo"<p>There was an error fetching your outstanding deliveries.</p>";
							?>
						</div>
					</div>
					<div class="inner">
						<p>All Orders</p>
						<div class="resultsbox">
							<?php
								if(count($unclaimedOrders) == 0)
								{
									echo "<p>There are no unclaimed orders.</p>";
								}
								else if(count($unclaimedOrders) > 0)
								{
									$cols = new stdClass();
									$cols->Comments      = true;
									$cols->OrderID       = true;
									$cols->CharacterName = true;
									$cols->OrderDate     = true;
									$cols->OrderType     = true;
									$cols->Destination   = true;
									$cols->Volume        = true;
									$cols->Price         = true;
									$cols->ViewOrder     = true;
									$cols->Claim         = true;
									OutputOrderList($unclaimedOrders, $cols);
									unset($cols);
								}
								else echo"<p>There was an error fetching the unclaimed orders.</p>";
							?>
						</div>
					</div>
					<div class='inner'>
						<p>All Courier Contracts</p>
						<div class='resultsbox'>
							<div class='table'>
								<?php
									if(count($couriers) == 0)
									{
										echo "<p>There are no submitted courier contracts.</p>";
									}
									else if(count($couriers) > 0)
									{
										$filter = new stdClass();
										$filter->Comments      = true;
										$filter->CourierID     = true;
										$filter->Volume        = true;
										$filter->Source        = true;
										$filter->Destination   = true;
										$filter->Complete      = true;
										$filter->CourierDate   = true;
										$filter->CharacterName = true;
										OutputCouriers($couriers, $filter);
										unset($filter);
									}
									else
									{
										echo "<p>There was an error getting the submitted courier contracts.</p>";
									}
								?>
							</div>
						</div>
					</div>
					<div class='inner'>
						<div class='resultsbox'>
							<p><u>Freighter Pilots</u></p>
							<p>On this page there are 3 sections, My Claimed Orders, All Orders, and All Couriers</p>
							<ul>
								<li><p>My Claimed Orders: These are orders that you have decided to buy and ship out to the desired destination.</p></li>
								<li><p>All Orders: These are orders that have not yet been claimed by a jumpfreighter pilot for delivery.</p></li>
								<li><p>All Couriers: These are courier contracts that have not yet been delivered. To Get a courier contract, talk to the person who placed it and get it assigned to yourself. Note: Courier contracts don't have a claimed status like orders do because two people can't both do the same contract ingame.</p></li>
							</ul>
							<p>Prices: All prices on this site are the <a href='http://forums.black-legion.us/viewtopic.php?f=28&t=916#p7681'>2.5th percentile of sell prices for The Forge region</a> and therefore do not take into account time, fuel, or any other expenses. All jumpfreighter pilots determine their own prices and when and how they get paid.</p>
						</div>
					</div>
					<?php
					}
					else
					{
					?>
					<div class='inner'>
						<div class='resultsbox'>
							<p>You are not currently setup as a jumpfreighter pilot. Perform the following steps to get the jumpfreighter role.</p>
							<ul>
								<li><p>Sit your jumpfreighter character in their jumpfreighter.</p></li>
								<li><p>Open this page in the ingame browser.</p></li>
								<li><p>When asked, set the page as trusted.</p></li>
								<li><p>Refresh the page, you should now have access to the jumpfreighters section.</p></li>
							</ul>
							<p>Note: The character you use to get the jumpfreighter roles does <b>not</b> have to be the same character you registered with</p>
						</div>
					</class>
					<?php
					}
					?>
				</div><!--middle-->
			</div>
		</div>
  </body>
</html>