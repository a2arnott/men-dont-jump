<?php
	include("private/logincheck.php");
	include("private/orderfns.php");
	include("private/general.php");
	$msg = '';
	$successmsg = '';
	
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$action   = isset($_POST['action'])  ? input($_POST['action'])  :false;
		$volume   = isset($_POST['volume'])  ? input($_POST['volume'])  :false;
		$source   = isset($_POST['source'])  ? input($_POST['source'])  :false;
		$dest     = isset($_POST['dest'])    ? input($_POST['dest'])    :false;
		$comments = isset($_POST['comments'])? input($_POST['comments']): null;
		
		if($action == false || $action != 'create') $msg = 'The specified action is not recognized.';
		else if($action == 'create' && ($volume == false || ! is_numeric($volume ) || floatval($volume) <= 0)) $msg = 'The specified volume is not valid.';
		else if($action == 'create' && ($source == false || empty( $source ))) $msg = 'The source was not specified.';
		else if($action == 'create' && ($dest   == false || empty( $dest ))) $msg = 'The destination was not specified.';
		else
		{
			$volume = floatval($volume);
			if(CreateCourier($volume, $source, $dest, $comments))
			{
				header("Location: couriers.php?status=success");
				exit;
			}
			else $msg = "The courier creation attempt failed, please try again.";
		}
	}
	else if($_SERVER['REQUEST_METHOD'] == 'GET')
	{
		if(isset($_GET['status']) && $_GET['status'] == 'success') $successmsg = "The courier request was successfully created. A jumpfreighter pilot will contact you shortly to get the contract assigned to themselves.";
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/general.css" />
		<title>Courier Contracts</title>
		<link rel="shortcut icon" href="images/favicon.ico" />
	</head>
	<body>
		<div class='gradient'>
			<?php include("private/header.php"); ?>
			<div class="outer">
				<div class="middle">
					<?php
						if($successmsg != '' || $msg != '')
						{
							echo "<div class='inner'><p>" . $successmsg . $msg . "</p></div>";
						}
					?>
					<div class="inner">
						<form action='couriers.php' method='post'>
						<input type='hidden' name='action' value='create' />
						<p>Create a new courier contract</p>
							<div class='table'>
								<div class='table-row'>
									<div class='table-cell'><p>Volume (m&#179;)</p></div>
									<div class='table-cell'><input type='number' name='volume' /></div>
								</div>
								<div class='table-row'>
									<div class='table-cell'><p>Pickup Location</p></div>
									<div class='table-cell'><input type='text' name='source' placeholder='From' title="Enter a station name, system, or anything else indicating where you want the courier picked up."/></div>
								</div>
								<div class='table-row'>
									<div class='table-cell'><p>Dropoff Location</p></div>
									<div class='table-cell'><input type='text' name='dest' placeholder='To' title="Enter a station name, system, or anything else indicating where you want the courier delivered."/></div>
								</div>
							</div><!--table-->
							<textarea maxlength='2048'  placeholder='Comments' name='comments' style='width:100%'></textarea>
							<div class='table'>
								<div class='table-row'>
									<div class='table-cell' style='width:100%'></div>
									<div class='table-cell'><input type='submit' value='Submit Courier' /></div>
								</div>
							</div>
						</form>
					</div>
					<div class='inner'>
						<div class='resultsbox'>
							<p><u>Creating a Courier Contract</u></p>
							<p>Couriers are simple to setup, you need to know the following.</p>
							<ul>
								<li><p>How big the stuff is</p></li>
								<li><p>Where the stuff will be picked up</p></li>
								<li><p>Where you want the stuff dropped off</p></li>
							</ul>
							<p>Once you have submitted a courier contract it goes in the list for jump freighter pilots to look at.</p>
							<p>When a jump freighter pilot wants to do your courier they will contact you, arrange payment, and get a contract assigned to themselves. From there it is business as usual.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>