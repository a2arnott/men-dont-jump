<!DOCTYPE html>
<html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="/css/general.css" />
		<title>Error</title>
		<link rel="shortcut icon" href="images/favicon.ico" />
	</head>
	<body>
		<div class='gradient'>
			<?php 
			include("private/session.php");
			if(isLoggedIn())
			{
				include("private/header.php");
			}
			?>
			<div class="outer">
				<div class="middle">
					<div class='inner'>
						<div class='resultsbox'>
							<p>There was an internal server error.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
