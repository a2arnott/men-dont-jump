<?php
	$stderr = fopen("php://stderr", "w");
	
	function getTypeIds()
	{
		$con = mysqli_connect(/*TODO ADD STUFF HERE */);
		if($con)
		{
			$command = "SELECT TypeID FROM Items WHERE NOT UpdateDate <=> current_date";
			$queryresults = mysqli_query($con, $command);
			$i = 0;
			while($results[$i] = mysqli_fetch_array($queryresults)) $i++;
			mysqli_free_result($queryresults);
			mysqli_close($con);
			return $results;
		}
		return null;
	}
	
	function InsertResults($results, $primaryquery)
	{
		$con = mysqli_connect(/*TODO ADD STUFF HERE */);
		$xml = new SimpleXMLElement($results);
		$root = $xml->marketstat;
		$retval = false;
		if($con)
		{
			if( isset($root->type))
			{
				$item = $root->type;
				if($item->sell->volume == 0) $retval = false;
				else $retval = true;
				
				$constant = 1.96;
				if(!$primaryquery) $constant = 1.28;
				
				
				$cost = max((floatval($item->sell->avg) - $constant * floatval($item->sell->stddev)),$item->sell->min);
				$updatestring  = "UPDATE Items SET Price = " . $cost . ", UpdateDate = current_timestamp WHERE TypeID = " . $item['id'];
				if(!mysqli_query($con, $updatestring))
				{
					echo "Error: " . $item["id"] . "\n";
				}
			}
			mysqli_close($con);
		}
		return $retval;
	}

	function PerformQuery($typeid, $primaryquery)
	{
		if($primaryquery) $queryurl = "http://api.eve-central.com/api/marketstat?regionlimit=10000002&typeid=" . $typeid;
		else $queryurl = "http://api.eve-central.com/api/marketstat?typeid=" . $typeid;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $queryurl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		
		#execute the query
		$results = curl_exec($ch);
		$retval = false;
		#now we need to itterate through the results
		if($results)
		{
			$retval = InsertResults($results, $primaryquery);
		}
		else
		{
			fprintf($stderr, "Error with query: " . $typeid);
		}
		curl_close($ch);
		return $retval;
	}


	function QueryEveCentral($typeids)
	{
		$rownum = 0;
		while($rownum != sizeof($typeids))
		{
			if(! PerformQuery($typeids[$rownum][0], true))
			{
				PerformQuery($typeids[$rownum][0], false);
			}
			$rownum++;
			usleep(10000); //so eve central doesn't hate us	
		}
	}


	$typeids = getTypeIds();
	QueryEveCentral($typeids);
?>
