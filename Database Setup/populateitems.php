<?php
	$xmlfile = simplexml_load_file("rootedtypeids.xml");
	$root = $xmlfile[0];
	
	$con = mysqli_connect("localhost", "evemmrvn_write", "test", "evemmrvn_JF");
	if($con)
	{
		foreach($root as $row)
		{
			$typeid = intval($row["TypeID"]);
			$groupid = intval($row["GroupID"]);
			$name = $row["ItemName"];
			
			$desc;
			if(isset($row["ItemDescription"])) $desc = $row["ItemDescription"];
			else $desc = "";
			
			$vol;
			if(isset($row["Volume"])) $vol = doubleval($row["Volume"]);
			else $vol = 0;
			
			$markgroup = intval($row["MarketGroupID"]);
			$date = date("Y-m-d");
			
			$insertcmd  = "INSERT INTO Items ( TypeID, GroupID, ItemName, ItemDescription, Volume, MarketGroupID, UpdateDate ) VALUES( " . $typeid . ", " . $groupid . ", '" . mysqli_real_escape_string($con, $name) . "', '" . mysqli_real_escape_string($con, $desc) . "', " . $vol . ", " . $markgroup . ", '" . $date . "')";
			$insertcmd .= " ON DUPLICATE KEY UPDATE GroupID = " . $groupid . ", ItemName = '" . mysqli_real_escape_string($con, $name) . "', ItemDescription = '', Volume = " . $vol . ", MarketGroupID = " . $markgroup . ", UpdateDate = '" . $date . "'";
			if(! mysqli_query($con, $insertcmd) )
			{
				echo "Error: " . mysqli_error($con);
			}
		}
		
		//now remove any items that weren't updated
		$removecmd = "DELETE FROM Items WHERE UpdateDate != '" . $date . "'";
		if(! mysqli_query($con, $removecmd))
		{
			echo "Error: " . mysqli_error($con);
		}
		mysqli_close($con);
	}
	else
	{
		echo mysqli_error($con);
	}
?>
