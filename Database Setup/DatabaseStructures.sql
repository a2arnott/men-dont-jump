CREATE TABLE Pilots
(
    CharacterID BIGINT NOT NULL,
    CharacterName VARCHAR(128) NOT NULL,
    IsJfPilot BIT DEFAULT 0,
    HashedPW VARCHAR(256),
    Salt VARCHAR(256),
    IsAdmin BIT DEFAULT 0,
    PRIMARY KEY (CharacterID)
)ENGINE=MyISAM;

CREATE TABLE AllowedAlliances
(
    AllianceID BIGINT NOT NULL,
    AllianceName Varchar(128),
    PRIMARY KEY(AllianceID)
)ENGINE=MyISAM;

CREATE TABLE Items (
    TypeID BIGINT NOT NULL,
    GroupID INT,
    ItemName VARCHAR(100),
    ItemDescription NVARCHAR(3000),
    Volume DECIMAL(15,2),
    MarketGroupID INT,
    Price DECIMAL(15,2),
    UpdateDate DATE,
    PRIMARY KEY (TypeID)
)ENGINE=MyISAM;

CREATE TABLE Orders
(
    OrderID INT NOT NULL AUTO_INCREMENT,
    CharacterID BIGINT NOT NULL,
    JFCharacterID BIGINT,
    Destination VARCHAR(128),
    Submitted BIT,
    Completed BIT,
    Cancelled BIT,
    OrderDate DATETIME,
    OrderType VARCHAR(64),
		Comments VARCHAR(2048),
    PRIMARY KEY(OrderID)
)ENGINE=MyISAM;

CREATE TABLE OrderItems
(
  OrderItemID INT NOT NULL AUTO_INCREMENT,
  TypeID BIGINT NOT NULL,
  Quantity INT NOT NULL,
  OrderID INT NOT NULL,
  PRIMARY KEY(OrderItemID)
)ENGINE=MyISAM;

CREATE VIEW OrderSummary AS
 SELECT Orders.OrderID, SUM(Price * Quantity) AS Price, SUM(Volume * Quantity) AS Volume, CharacterID, Destination, JFCharacterID, OrderDate, Submitted, Completed, Cancelled, OrderType, Comments
  FROM (Orders JOIN OrderItems ON Orders.OrderID = OrderItems.OrderID) JOIN Items ON OrderItems.TypeID = Items.TypeID
   GROUP BY Orders.OrderID;
  
CREATE TABLE ServicedLocations
(
    SystemName VARCHAR(128),
    PRIMARY KEY(SystemName)
)ENGINE=MyISAM;
  
CREATE TABLE Couriers
(
    CourierID INT NOT NULL AUTO_INCREMENT,
    CharacterID BIGINT NOT NULL,
    Volume INT NOT NULL,
    Source VARCHAR(128),
    Destination VARCHAR(128),
    JFCharacterID BIGINT DEFAULT NULL,
    Comments VARCHAR(2048),
    Completed BIT DEFAULT 0,
    Cancelled BIT DEFAULT 0,
    CourierDate DATE NOT NULL,
    CompletionDate DATE DEFAULT NULL,
    PRIMARY KEY(CourierID)
)ENGINE=MyISAM;

--DONE TO HERE

CREATE TABLE Complaints
(
    ComplaintID INT NOT NULL AUTO_INCREMENT,
    ItemID INT NOT NULL,
    Comments VARCHAR(2048),
    ComplaintType INT,
    PRIMARY KEY(ComplaintID)
)ENGINE=MyISAM;

CREATE TABLE ComplaintTypes
(
    ComplaintTypeID INT NOT NULL AUTO_INCREMENT,
    ComplaintTypeName VARCHAR(128),
    PRIMARY KEY( ComplaintTypeID)
)ENGINE=MyISAM;
